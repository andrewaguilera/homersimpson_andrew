package com.example.homersimpson_andrew;

import android.animation.AnimatorSet;
import android.graphics.drawable.AnimationDrawable;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    ImageView simpson_0_1_2_ImageView,
            ull_ImageView, donut_ImageView,
            engranatge_vermell_ImageView, engranatge_blau_ImageView, engranatge_verd_ImageView;

    Boolean imageVisibility;

    Animation rotateclock_animation;
    Animation rotateanticlock_animation;

    MediaPlayer mediaPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //B -TITLE ANIMATION simpson_0_1_2_ImageView.
        simpson_0_1_2_ImageView = findViewById(R.id.simpsons_0_1_2_ImageView);
        AnimationDrawable simpson_0_1_2_animation = (AnimationDrawable) simpson_0_1_2_ImageView.getDrawable();
        simpson_0_1_2_animation.start();

        //C -APPEAR AND DISAPEAR ELEMENTS.
        ull_ImageView = findViewById(R.id.ull_ImageView);
        donut_ImageView = findViewById(R.id.donut_ImageView);
        engranatge_blau_ImageView = findViewById(R.id.engranatge_blau_ImageView);
        engranatge_verd_ImageView = findViewById(R.id.engranatge_verd_ImageView);
        engranatge_vermell_ImageView = findViewById(R.id.engranatge_vermell_ImageView);

        simpson_0_1_2_ImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(imageVisibility){
                    setImagesInvisible();
                }
                else{
                    setImagesVisible();
                 }
            }
        });

        //D -ROTATE "ENGRANATGES".
        setImagesVisible();

        //G -MUSIC
        donut_ImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try
                {
                    if (mediaPlayer != null && mediaPlayer.isPlaying())
                    {
                        mediaPlayer.stop();
                    }
                    else
                    {
                        mediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.the_simpsons);
                        mediaPlayer.start();
                    }
                } catch(Exception e) { /**/}
            }
        });
    }

    private void setImagesInvisible(){
        engranatge_blau_ImageView.clearAnimation();
        engranatge_verd_ImageView.clearAnimation();
        engranatge_vermell_ImageView.clearAnimation();
        donut_ImageView.clearAnimation();
        ull_ImageView.clearAnimation();

        ull_ImageView.setVisibility(View.INVISIBLE);
        donut_ImageView.setVisibility(View.INVISIBLE);
        engranatge_blau_ImageView.setVisibility(View.INVISIBLE);
        engranatge_verd_ImageView.setVisibility(View.INVISIBLE);
        engranatge_vermell_ImageView.setVisibility(View.INVISIBLE);
        imageVisibility = false;
    }

    private void setImagesVisible(){
        ull_ImageView.setVisibility(View.VISIBLE);
        donut_ImageView.setVisibility(View.VISIBLE);
        engranatge_blau_ImageView.setVisibility(View.VISIBLE);
        engranatge_verd_ImageView.setVisibility(View.VISIBLE);
        engranatge_vermell_ImageView.setVisibility(View.VISIBLE);

        Animation rotateclock_animation = AnimationUtils.loadAnimation(this, R.anim.animation_rotateclock);
        Animation rotateanticlock_animation = AnimationUtils.loadAnimation(this, R.anim.animation_rotateanticlock);
        Animation upanddown_animation = AnimationUtils.loadAnimation(this, R.anim.animation_moveupanddown);
        Animation donut_animation = AnimationUtils.loadAnimation(this, R.anim.animation_donut);
        Animation ull_animation = AnimationUtils.loadAnimation(this, R.anim.animation_eyerotation);

        rotateclock_animation.setInterpolator(new LinearInterpolator());
        rotateanticlock_animation.setInterpolator(new LinearInterpolator());
        upanddown_animation.setInterpolator(new LinearInterpolator());
        donut_animation.setInterpolator(new LinearInterpolator());
        ull_animation.setInterpolator(new LinearInterpolator());

        //AnimatorSet aimatorSet

        engranatge_blau_ImageView.startAnimation(rotateanticlock_animation);
        engranatge_verd_ImageView.startAnimation(rotateclock_animation);
        ull_ImageView.startAnimation(ull_animation);
        engranatge_vermell_ImageView.startAnimation(rotateanticlock_animation);
        donut_ImageView.startAnimation(donut_animation);
        imageVisibility = true;
    }
}
